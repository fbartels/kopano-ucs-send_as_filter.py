# kopano `send_as_filter.py`

By default Postfix from Univention allow to send email from every address for authenticated users.
It could be solved by
`ucr set mail /postfix/smtpd/restrictions/recipient/20 = "reject_authenticated_sender_login_mismatch"`

ut this is not a good solution if you use Kopano and you use smtp/imap configured mail clients.

Kopano adding additional custom schemas to ldap such as k4uUserSendAsPrivilege that allow user to send email as other user. It’s works only if you use web interface. Postfix has no checking scripts for this.

So if you implemented `reject_authenticated_sender_login_mismatch` to close the bug , you will not able to use "UserSendAsPrivilege " by smtp.

==================


# `send_as_filter.py` install


`send_as_filter.py` based on `listfilter.py` written by kopano team.  And my sccript requed some parts, for example `/etc/listfilter.secret`  file.

You shoud enable `listfilter.py`  before you can use `send_as_filter.py`



commands

```
ucr set mail/postfix/policy/listfilter=yes
# fix bug with webmail kopano client, users are not authenticated
ucr set mail/postfix/policy/listfilter/use_sasl_username=false
/etc/init.d/postfix restart
```


Now `send_as_filter.py`

/etc/postfix/master.cf.local

```
#dc=yourdomain,dc=local  is your UCS ldap domain, not mail domain
send_as_filter     unix  -       n       n       -       30       spawn user=listfilter argv=/usr/share/univention-mail-postfix/send_as_filter.py
    -b dc=yourdomain,dc=local
```

/etc/postfix/main.cf.local

```
#if something goes wrong with send_as_filter.py script  will not broke postfix service
smtpd_policy_service_default_action=DUNNO
```

commands

```
ucr set  mail/postfix/smtpd/restrictions/recipient/20="check_policy_service unix:private/send_as_filter"
univention-config-registry commit /etc/postfix/main.cf
univention-config-registry commit /etc/postfix/master.cf
service postfix reload

```
if you want all some users to send email from every address use this:

```
ucr set mail/send_from_any_email_users=user1 user2 user3 etc4
```
